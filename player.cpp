#include "player.h"
#define LARGE_NUMBER 1000000
#define MAXPLY 5

const int Player::MULTIPLIERS[][8] = {{50, -10, 5, 5, 5, 5, -10, 50},
                       {-10, -20, 1, 1, 1, 1, -20, -10},
                       {5, 1, 1, 1, 1, 1, 1, 5},
                       {5, 1, 1, 1, 1, 1, 1, 5},
                       {5, 1, 1, 1, 1, 1, 1, 5},
                       {5, 1, 1, 1, 1, 1, 1, 5},
                       {-10, -20, 1, 1, 1, 1, -20, -10},
                       {50, -10, 5, 5, 5, 5, -10, 50}};

/*
 * Constructor for the player; initialize everything here. The side your AI is
 * on (BLACK or WHITE) is passed in as "side". The constructor must finish 
 * within 30 seconds.
 */
Player::Player(Side _side) {

    // Will be set to true in test_minimax.cpp.
    testingMinimax = true;

     std::cerr << "Constructing player..." << std::endl;
     side = _side;
     opponentsSide = side == BLACK ? WHITE : BLACK;
     currBoard = new Board();

}

/*
 * Destructor for the player.
 */
Player::~Player() {
}

/*
 * Compute the next move given the opponent's last move. Your AI is
 * expected to keep track of the board on its own. If this is the first move,
 * or if the opponent passed on the last move, then opponentsMove will be NULL.
 *
 * msLeft represents the time your AI has left for the total game, in
 * milliseconds. doMove() must take no longer than msLeft, or your AI will
 * be disqualified! An msLeft value of -1 indicates no time limit.
 *
 * The move returned must be legal; if there are no valid moves for your side,
 * return NULL.
 */
Move *Player::doMove(Move *opponentsMove, int msLeft) {

    // Perform opponent's move.
    currBoard->doMove(opponentsMove, opponentsSide);

    // If there are no available moves, we have to pass.
    if (!currBoard->hasMoves(side)) return NULL;

    Move *nextMove;
    std::vector<Move*> candidates;

    // Compile a vector of all legal moves.
    for (int i = 0; i < 8; i++) {
        for (int j = 0; j < 8; j++) {
            Move *move = new Move(i, j);
            if (currBoard->checkMove(move, side))
                candidates.push_back(move);
        }
    }

    // Decide what method to use to compute the next move, and execute.
    switch (testingMinimax)
    {
        case false:
            nextMove = heuristicMove(candidates);
            break;
        case true:
            nextMove = miniMaxMove(candidates);
            break;
    }

    // Free memory from unused moves.
    for (std::vector<Move*>::iterator curr = candidates.begin(); curr != candidates.end(); curr++)
    {
    	if (*curr != nextMove)
    		delete *curr;
    }

    // Perform move on local board.
    currBoard->doMove(nextMove, side);

    return nextMove;
}

// Compute next possible move using a heuristic algorithm.
Move *Player::heuristicMove(std::vector<Move*> candidates) {
    //variable declarations
    Move *possibleMove = NULL;
    Move *finalMove = NULL;
    int xc;
    int yc;
    int multiplier;
    int result = 0;
    //we use large numbers to guarantee the condition is true upon the first loop later on.
    int result2 = -1*LARGE_NUMBER;
    int result3 = LARGE_NUMBER;
    bool beginning = true;
    Board *copyOfBoard;
    //Check to see if we have more than 8 tiles on the board when considering the move
    //If it is very early in the game, we want to get as little tiles as possible (strategy!)
    if ((currBoard->count(side) > 8) && (currBoard->countBlack() + currBoard->countWhite() > 20)) 
    {
        beginning = false;
    }
    //Loop through all possible moves
    while (!candidates.empty()) {
        possibleMove = candidates.back();
        xc = possibleMove->x;
        yc = possibleMove->y;
        candidates.pop_back();
        multiplier = MULTIPLIERS[xc][yc];
        
        //make a copy of the curren board so we can work with
        copyOfBoard = currBoard->copy();
        copyOfBoard->doMove(possibleMove, side); 
        
        //If it is early in the game, make move which minimizes our tile count
        if (beginning)
        {
            result = copyOfBoard->count(side);
            if (result < result3)
            {
                result3 = result;
                finalMove = possibleMove;
            }
        }
        //Otherwise, choose the move which has the highest count * multiplier value 
        //This is based on the count after the move is made * a multiplier dependent on the position of the move
        else
        {
            result = copyOfBoard->count(side)*multiplier;
            if (result > result2)
            {
                result2 = result; 
                finalMove = possibleMove;    
            }
        }           
    }
    delete copyOfBoard;

    //return the move, board is updated elsewhere
    return finalMove;
}

// Compute next possible move using a minimax algorithm.
Move *Player::miniMaxMove(std::vector<Move*> candidates) {
    Move *bestMove = NULL;
    int bestWorstScore, worstScore;

    // Work our way through the vector of possible moves.
    for (std::vector<Move*>::iterator curr = candidates.begin(); curr != candidates.end(); curr++)
    {
        // For each move, create a copy of the board and try playing that move.
        Board *level1 = currBoard->copy();
        level1->doMove(*curr, side);
        worstScore = recMiniMax(level1, 1);

        // Update best move if we found a better one.
        if (bestMove == NULL || worstScore > bestWorstScore)
        {
            bestMove = *curr;
            bestWorstScore = worstScore;
        }
        delete level1;
    }

    return bestMove;
}

int Player::recMiniMax(Board *board, int iter) {
	int worstScore;
    bool initialized = false;
	iter++;

	// If the board has moves, find the worst possible outcome.
    if (board->hasMoves(side))
    {
    	// Do so by trying every space on the board
    	for (int i = 0; i < 8; i++) {
    		for (int j = 0; j < 8; j++) {
    			Move move(i, j);
    			// And if that space is a valid move
    			if (board->checkMove(&move, opponentsSide))
                {
                    // Play that move on a copy on the board.
                    Board *newlevel = board->copy();
                    newlevel->doMove(&move, opponentsSide);
                    int curr;
                    if (iter < MAXPLY)
                    {
                    	curr = recMiniMax(newlevel, iter);
                    }
                    else
                    {
                    	curr = relativeScore(newlevel);
                    }
                    // Save resulting score if it is the minimum so far.
                    if (initialized == false || curr < worstScore)
                    {
                        worstScore = curr;
                        initialized = true;
                    }
                    delete newlevel;
                }
            }
        }
    }
    else
    {
        // If there are no possible moves, the worst score is this level's score.
        worstScore = relativeScore(board);
    }
    return worstScore;
}

int Player::relativeScore(Board* board) {
    int score = 0;
    int score2 = 0;
    for (int i = 0; i < 8; i++) 
    {
        for (int j = 0; j < 8; j++)
        {
            int multiplier = MULTIPLIERS[i][j];
            if (board->gettile(side, i, j))
            {
                score += multiplier* board->count(side);
            }
            else if (board->gettile(opponentsSide, i, j))
            {
                score2 += multiplier* board->count(opponentsSide);
            }
        }
    }
    return score - score2;
    //return board->count(side) - board->count(opponentsSide);
}
