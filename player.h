#ifndef __PLAYER_H__
#define __PLAYER_H__

#include <iostream>
#include "common.h"
#include "board.h"
#include <cmath>
#include <vector>
using namespace std;

class Player {

private:
    static const int MULTIPLIERS[8][8];

public:
    Player(Side side);
    ~Player();
    
    Move *doMove(Move *opponentsMove, int msLeft);

    Move *heuristicMove(std::vector<Move*> candidates);
    Move *miniMaxMove(std::vector<Move*> candidates);
    int recMiniMax(Board *board, int iter);

    int relativeScore (Board *board);

    // Flag to tell if the player is running within the test_minimax context
    bool testingMinimax;

    Board *currBoard;
    Side side;
    Side opponentsSide;

    // Change the player's board's state.
    void setBoard(char boardData[]) { currBoard->setBoard(boardData); }
};

#endif
