CS2 - Assignment 10
Santiago Navonne and Jean-Alexandre Turban

Team member contributions:
Santiago worked on main move processing and minimax algorithms. Jean worked on heuristic
functions and score computation procedures. We both did a fair amount of joint debugging, 
sitting in the same room, as well as joint discussions to decide how to implement our algorithms.


The first step we took was writing a simple heuristic function which calculated the current score minus our opponents score. This was quickly changed however, as it struggled to beat even SimplePlayer. Our next heuristic function took into account some multipliers based on the position of the move that we were considering making. Following this we added some more strategy to the heuristic function by taking into account whether it was the beginning of the game or not - our heuristic strategy at this point was as follows: if it was the beginning of the game, we wanted to minimize our current tile-count, otherwise we wanted to maximize our heuristic score using the multipliers. Edges and corners were weighted more heavily. 
Following the heuristic function we made our AI better by implementing the minimax. Our first version of this was two-ply and weighted the score of the board based on our current tile count. Following this, we made the AI with a 4/5 ply minimax where it calculated the score board while taking into account the weight of each tile position.

